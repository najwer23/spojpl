#include <iostream>
#include <string>
#include <cmath>
using namespace std;

//stworzenie konstruktora kopiujacego, pozwala skopiowa adres
// a nie tylko wartosci, ktore kopiuja sie automatycznie
class Punkt
{
private:
    string nazwa;
    int x;
    int y;
    double dlugosc;
public:
    void OdlegloscOdZera(Punkt p);
    Punkt();
    ~Punkt();
    friend void bombelkowe(Punkt tab[], int rozmiar);
    friend ostream& operator<< (ostream &wyjscie, const Punkt &p);
    friend istream& operator>> (istream &wejscie, Punkt &p);
};

void Punkt::OdlegloscOdZera(Punkt p)
{
    dlugosc=(sqrt(x*x+y*y));
}
istream& operator >>(istream &wejscie, Punkt &p)
{
    wejscie>>p.nazwa>>p.x>>p.y;
    return wejscie;
}

void bombelkowe(Punkt tab[], int rozmiar)
{
    for(int i=0; i<rozmiar; i++)
        for(int j=1; j<rozmiar-i; j++)
            if(tab[j-1].dlugosc>tab[j].dlugosc)
                //zamiana miejscami
                swap(tab[j-1], tab[j]);
}

ostream& operator << (ostream &wyjscie, Punkt const &p)
{
    wyjscie<<p.nazwa<<" "<<p.x<<" "<<p.y<<endl;
    return wyjscie;
}

//konstrktor
Punkt::Punkt()
    :nazwa ( "0" )
    ,x ( 0 )
    ,y ( 0 )
    ,dlugosc (0)
{
    ;
}

//destruktor
Punkt::~Punkt()
{
    ;//?
}

int main()
{
    int rozmiar;
    int ile;

    cin>>ile;
    while(ile--)
    {
        cin>>rozmiar;
        Punkt Tablica[rozmiar];
        Punkt punkt;
        for(int i=0; i<rozmiar; i++)
        {
            cin>>punkt;
            punkt.OdlegloscOdZera(punkt);
            Tablica[i]=punkt;
        }

        bombelkowe(Tablica,rozmiar);

        for(int i=0; i<rozmiar; i++)
        {
            cout<<Tablica[i];
        }
    }
    return 0;
}
