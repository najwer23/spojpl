#include <iostream>
using namespace std;
#define PI 3.141592654

double pole_kola(double promien, double odleglosc)
{
    double pole;
    pole=(promien*promien-((odleglosc/2)*(odleglosc/2)))*PI;
    return pole;
}

void wyswietl(double pole)
{
    cout.precision(2);
    cout<<fixed;
    cout<<pole;
}
int main()
{
    double OdlegloscSrodkow;
    double PromienSfery;
    double PoleKola=0;

    cin>>PromienSfery;
    cin>>OdlegloscSrodkow;

    if((2*PromienSfery>OdlegloscSrodkow) && (OdlegloscSrodkow>0))
    {
        PoleKola=pole_kola(PromienSfery,OdlegloscSrodkow);
        wyswietl(PoleKola);
    }

    if((2*PromienSfery<=OdlegloscSrodkow) || (OdlegloscSrodkow==0))
        wyswietl(0);

    return 0;
}
